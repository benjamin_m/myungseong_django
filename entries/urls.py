#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url, include
from entries.models import Entry, News
from django.views.generic.list import ListView
from django.views.generic.edit import UpdateView, DeleteView, CreateView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView

info_dict = {
    'queryset': Entry.objects.all(),
}

urlpatterns = patterns('',
                       # select
                       (r'^$', ListView.as_view(queryset=News.objects.all())),
                       # BBS
                       # create
                       (r'^create/$',
                        CreateView.as_view(model=Entry, success_url='/entries/bbs/')),
                       # edit
                       (r'^detail/(?P<pk>\d+)/$',
                        DetailView.as_view(queryset=Entry.objects.all())),
                       # update
                       (r'^update/(?P<pk>\d+)/$',
                        UpdateView.as_view(model=Entry)),
                       # delete
                       (r'^delete/(?P<pk>\d+)/$',
                        DeleteView.as_view(model=Entry, success_url='/entries/bbs/')),
                       # NEWS
                       # create
                       (r'^create_news/$',
                        CreateView.as_view(model=News, success_url='/entries/')),
                       # edit
                       (r'^detail_news/(?P<pk>\d+)/$',
                        DetailView.as_view(queryset=News.objects.all())),
                       # update
                       (r'^update_news/(?P<pk>\d+)/$',
                        UpdateView.as_view(model=News)),
                       # delete
                       (r'^delete_news/(?P<pk>\d+)/$',
                        DeleteView.as_view(model=News, success_url='/entries/')),

                       # normal pages
                       (r'^mail/', TemplateView.as_view(template_name="pages/mail.html")),
                       (r'^member/', TemplateView.as_view(template_name="pages/member.html")),
                       (r'^bbs/', ListView.as_view(queryset=Entry.objects.all())),
                       (r'^schedule/', TemplateView.as_view(template_name="pages/schedule.html")),
                       (r'^master/', TemplateView.as_view(template_name="pages/master.html")),
                       (r'^links/', TemplateView.as_view(template_name="pages/links.html")),
                       (r'^edogawa/', TemplateView.as_view(template_name="pages/edogawa.html")),
                       (r'^shinjukuclub/', TemplateView.as_view(template_name="pages/shinjukuclub.html")),
                       (r'^mail/', TemplateView.as_view(template_name="pages/mail.html")),
                       # comment for BBS
                       (r'^comments/', include('django.contrib.comments.urls')),
                       # base.htmlのプルダウン言語セットに必須
                       (r'^i18n/', include('django.conf.urls.i18n')),
)

# login / logout
urlpatterns += patterns('',
                        (r'login/$', 'django.contrib.auth.views.login',
                         {'template_name': 'entries/registration/login.html'}),
                        (r'^logout/$',
                         'django.contrib.auth.views.logout',
                         {'template_name': 'entries/registration/logout.html'}),
)
